#!/usr/bin/perl
#print "The number of command line arguements is @{[$#ARGV+1]}.\n";
#Note: doesn't get the call <... lgetfilecon resumed> and other such calls
if ($#ARGV < 1)
{
	print "You require an input file (first arg) and output file (second arg)\n";
	exit;
}
open FILE, $ARGV[0];
my $filename = $ARGV[1];
open(my $fh, '>', $filename) or die "Could not open the file '$filename' $!";
while ($line=<FILE>){
	if ($line=~/^(.*?) (.*?) \[(.*?)\] (.*?)\(.*<(.*?)>$/){
		print $fh "$1,$2,$3,$4,$5\n";
	}
}
close $fh;
print "done formatting the input file to '$filename'\n";
