#!/usr/bin/bash
len=$#;#The number of input file
arr=("$@");
var=${arr[$len-1]};#last argument is the output file
unset arr[${#arr[@]}-1];
echo "ltrace -o $var -TSifCttt ${arr[@]}";
#To execute the instruction, have to print it out for some reason.
#Send output to /dev/null (delete it)
if (($len < 2))#Can have more than 2 arguments, but if no valid output file then it will still output
then
	echo "The supplied number of arguments is less than 2: $len";
	exit;
fi
{echo `ltrace -o $var -TSifCttt ${arr[@]}`} &>/dev/null;
