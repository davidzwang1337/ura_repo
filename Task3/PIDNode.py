class PIDNode:
	PIDcount = 0#Holds the total number of PIDs (all ever seen). Never decrements
	
	def __init__(self,PID,time,callCount):
		self.PID = PID
		self.startTime = time
		self.startCall = callCount
		self.children = []#Holds the children nodes
		self.endCall = None
		self.endTime = None
		
	def __repr__(self):
		#toStr = lambda x: str(x)
		#return ', '.join(map(toStr,[self.PID,self.startTime,self.startCall,self.children,self.endCall,self.endTime]))
		return str(vars(self))
	
	def __str__(self):
		return str(self.PID)
		
	def addParent(parPID):
		self.parentPID = parPID
		
	def addChild(PID):
		self.children.append(PID)

