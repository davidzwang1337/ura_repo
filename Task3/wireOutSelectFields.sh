#!/usr/bin/bash

#https://www.wireshark.org/docs/dfref/#section_i for field names
#Standard csv dump is: frame.number,frame.time_relative,ip.src,ip.dst,ip.proto,frame.len,col.Info
#To get col.Info field, requires development release 1.99.1 (as of January 2015)

#NOTE: This current setup only prints out the ip traffic. For network traffic, requires changing of fields

#Might want to go back and replace protocol number with their names (6:TCP,17:UDP,112:VRRP)

$(tshark -r $1 -T fields -e frame.number -e frame.time_relative \
-e ip.src -e ip.dst -e ip.proto -e frame.len -E \
header=y -E separator=, -E quote=d -E occurrence=f > /tmp/mainInfo.csv)

#For some reason, col.Info doesn't work even with development version
#Use tshark -r file.pcap -o 'column.format:"Info","%i"' to a temporary file
#Then append each line of that temp file to the main file

echo "col.Info" > /tmp/colInfo.csv

$(tshark -r $1 -o 'column.format:"Info","%i"' >> /tmp/colInfo.csv)

#Append lines from each file and add first double quote
#/dev/null required since only one delimiter allowed each file
paste -d ',"' /tmp/mainInfo.csv /dev/null /tmp/colInfo.csv > $2

#Add second double quote at the end of the merged line
sed -e 's/$/"/' -i $2
