#!/usr/bin/perl
open(RAW,"tshark -r $ARGV[0] |") || die "Opening the file failed: $!\n";
open(my $out, '>', $ARGV[1]) or die "Could not open the file '$filename' $!";
while ($line=<RAW>)
{
	if ($line=~/^\s*(\d+)\s*(.*?) (.*?) -> (.*?) (.*?) (.*)$/)
	{
		#print "$1,$2,$3,$4,$5,$6\n";
		print $out "\"$1\",\"$2\",\"$3\",\"$4\",\"$5\",\"$6\"\n";
	}
}
close $out;

#To input into R directly by calling the script:
#fileLines <- system("perl processRaw.pl dump3.pcapng blank.txt", intern=TRUE)
