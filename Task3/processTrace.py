#/usr/bin/python
import re
import sys
import time
import cPickle as pickle
from PIDNode import PIDNode

if len(sys.argv) != 3:
	print 'The number of arguments you have supplied is ' + str(len(sys.argv)-1)
	print 'You need to specify two arguments: the input file and the outputfile'
	sys.exit()
	
inFile = open(sys.argv[1],"r")

#If it is the first occurence of the PID then create a node and add it to the PID list
#The PIDList contains all of the PIDNode objects
#Assumptions: adds should be chronological, so when you finally process the tree
#you keep an ind of where you are in the list and look at the remainder of the list forwards

#Processes are assumed to have been created at the end of clone calls (when they return)
#In reality, they might execute some instructions before the clone entirely returns,
#but the difference in time is negligible compared to the length of the entire trace

#Process exits on exit() call. And exit_group call. All threads share one PID, so just terminate that one
#When exits, delete from used PID list

#When refering to the syscall clone, it will be sysclone
#When fork or clone returns on the child process it returns 0
#When clone returns without exit beforehand, then it terminates the program
#If -1 ever returned on parent process, then child process was not created successfully
#But sysclone will always return. But how to differentiate non-sysclone from clone calls...
#Hope application writer only uses fork which calls clone@SYS

PIDList = []#Stores the node PIDs
active = []#Ones for everything except PIDs that exited
NodeList = []#Stores the actual nodes
inc = 0
patt1 = '^(.+?) (.+?) \[(.+?)\] <... (.+?) resumed> .*?\)\s+=\s+<?(.+?)>?\s+<(.+?)>$'
patt2 = '^(.+?) (.+?) \[(.+?)\] (.+?)\(.*\)\s+=\s+<?(.+?)>?\s+<(.+?)>$'
patt3 = '^(.+?) (.+?) \[(.+?)\]\s+\++\s+exited \(status \d+\)\s+\++$'#Checks if a PID exited
prog1 = re.compile(patt1)
prog2 = re.compile(patt2)
prog3 = re.compile(patt3)
startTime = time.time()

for line in inFile:
	inc += 1
	a = prog1.match(line)
	if a is not None:
		PID = int(a.group(1))
		if PID not in PIDList:
			PIDList.append(PID)
			active.append(1)
			node = PIDNode(PID,a.group(2),inc)
			NodeList.append(node)
		if a.group(4).startswith("clone"):
			addr = int(a.group(5),16)#Convert from hex
			for i,key in enumerate(PIDList):
				if key == PID and active[i] == 1:
					NodeList[i].children.append(addr)
	else:
		a = prog2.match(line)
		if a is not None:
			PID = int(a.group(1))
			if PID not in PIDList:
				PIDList.append(PID)
				active.append(1)
				node = PIDNode(PID,a.group(2),inc)
				NodeList.append(node)
			if a.group(4).startswith("clone"):
				addr = int(a.group(5),16)#Convert from hex
				for i,key in enumerate(PIDList):
					if key == PID and active[i] == 1:
						NodeList[i].children.append(addr)
		else:
			b = prog3.match(line)
			if b is not None:
				exited = 0
				PID = int(b.group(1))
				for i in range(len(PIDList)):
						if PIDList[i] == PID:
							active[i] = 0#Set the PID to not be active
							NodeList[i].endCall = inc
							NodeList[i].endTime = b.group(2)
							exited += 1
				assert exited == 1

#print NodeList
#print NodeList[0].__dict__.keys().sort()
#print vars(NodeList[0]).keys()
elapsedTime = time.time() - startTime;

print "Total time to process: " + str(elapsedTime)

pickle.dump(NodeList,open(sys.argv[2],"wb"))

inFile.close()
