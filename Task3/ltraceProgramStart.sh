#!/usr/bin/bash
#Executes the command and immediately attaches ltrace onto the program PID

#NOTE: ltrace causes the program to run much slower than usual. At least 10x slower

len=$#;#The number of input file
arr=("$@");
var=${arr[$len-1]};#last argument is the output file
unset arr[${#arr[@]}-1];
#To execute the instruction, have to print it out for some reason.
#Send output to /dev/null (delete it)
if (($len < 2))#Can have more than 2 arguments, but if no valid output file then it will still output
then
	echo "The supplied number of arguments is less than 2: $len";
	exit;
fi
echo "${arr[@]} &"
${arr[@]} &
echo $!
#echo "ltrace -o $var -TSifCtttp $!";
{echo `ltrace -o $var -TSifCtttp $!`} &>/dev/null;
#Note, if it is a short program like ls then it might return before ltrace can attach
#use ltraceProgram.sh for that case if possible
