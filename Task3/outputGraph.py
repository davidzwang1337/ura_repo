import sys
import os
import cPickle as pickle

if len(sys.argv) != 4:
	print 'The number of arguments you have supplied is ' + str(len(sys.argv)-1)
	print 'You need to specify two arguments: the input file and the output graph'
	sys.exit()

NodeList = pickle.load(open(sys.argv[1],'rb'))
graphFile = open(sys.argv[2],"wb")

#Process and create the time nodes first

times = []
for node in NodeList:
	#print node.PID, node.startTime, node.endTime, node.startCall, node.endCall
	times.append(float(node.startTime))
	if node.endTime != None:
		times.append(float(node.endTime))
	
times = sorted(times)
relTime = []
relTime[:] = [x-times[0] for x in times]
#print times

#Create the graph and process the time sidebar

graphFile.write("digraph trace{\n{\nnode[shape=plaintext,fontsize=12];\n");
for i in range(len(times)):
	time = ('%.5f' % (times[i]))
	#print time
	graphFile.write(time+"[label="+str(relTime[i])+"];\n")
for i in range(len(times)):
	time = ('%.5f' % (times[i]))
	graphFile.write(time + " -> ")
	#print time
graphFile.write("furtherOn;\n}\n")

#Go through the nodes and create the start nodes

graphFile.write("{\nnode[shape=polygon,sides=4,color=lightblue,distortion=-0.5];\n")
for node in NodeList:
	graphFile.write(str(node.PID)+";")
graphFile.write("\n}\n")

#Create the end nodes if the process has terminated in the middle of the ltrace

graphFile.write("{\nnode[shape=polygon,sides=4,color=red,distortion=0.3];\n")
for node in NodeList:
	if node.endTime != None:
		graphFile.write("\"" + str(node.PID)+"END\";")
graphFile.write("\n}\n")

#Assumes that main parent node is first node, then appends all of the children nodes

main = str(NodeList[0].PID)

for child in NodeList[1:]:
	if child.endTime != None:
		endPID = "\"" + str(child.PID)+"END\";\n"
		graphFile.write(main + " -> " + str(child.PID) + " -> " + endPID)
	else:
		graphFile.write(main + " -> " + str(child.PID) + ";\n")

#Set the ranks of the PIDs to be the same as their times

for node in NodeList:
	if node.endTime != None:
		endPID = ";\"" + str(node.PID)+"END\";}\n"
		time = ('%.5f' % float(node.endTime))
		graphFile.write("{rank = same;" + time + endPID)
	PID = ";" + str(node.PID)+";}\n"
	time = ('%.5f' % float(node.startTime))
	graphFile.write("{rank = same;" + time + PID)
		

#{rank = same; 0; main;}
graphFile.write("\n}");
graphFile.close()

os.system("dot -Tpdf " + sys.argv[2] + " -o " + sys.argv[3])
