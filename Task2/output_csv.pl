#!/usr/bin/perl
#print "The number of command line arguements is @{[$#ARGV+1]}.\n";
#Note: doesn't get the call <... lgetfilecon resumed> and other such calls
if ($#ARGV < 1)
{
	print "You require an input file (first arg) and output file (second arg)\n";
	exit;
}
open FILE, $ARGV[0];
my $filename = $ARGV[1];
$started = 0, $ended = 0;
open(my $fh, '>', $filename) or die "Could not open the file '$filename' $!";
while ($line=<FILE>){
	if ($line=~/^(.+?) (.+?) \[(.+?)\] (.+?)\(.*<unfinished ...>$/){#Unfinished process
		print $fh "$1,$2,$3,$4-START,NA\n";
		$started++;
	}
	elsif ($line=~/^(.+?) (.+?) \[(.+?)\] <... (.+?) resumed> \).*<(.+?)>$/){#Process resumed and finished
		print $fh "$1,$2,$3,$4-END,$5\n";
		$ended++;
	}
	elsif ($line=~/^(.+?) (.+?) \[(.+?)\] (.+?)\(.*<(.+?)>$/){#Normal process that started and finished
		print $fh "$1,$2,$3,$4,$5\n";
	}
}
close $fh;
print "done formatting the input file to '$filename'\n";
print "The number of started calls is $started. The number of ended calls is $ended \n";

#Note some processes are interrupted. If the time is <unfinished ...> then add a "-START"
#to the end of the process name, and NA as the time
#Then when in the process it says <...PROCESS_NAME resumed>, for the process name use
#PROCESS_NAME-END, calltime

#101000,1421981062.914034,0x3cc7038d48,g_str_hash-START,NA
#101000,1421981062.914034,0x3cc7038d48,g_str_hash-END,0.000352


#10564 1421984610.468063 [0x4090f1] __lxstat(1, "multiplePIDHistogram.R", 0x110d6d0 <unfinished ...>
#10564 1421984610.468298 [0x4090f1] <... __lxstat resumed> ) = 0 <0.000234>
