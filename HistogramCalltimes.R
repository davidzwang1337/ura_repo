input = read.csv("emacs-1.csv",header=F)
colnames(input) <- c("pid", "systime","proc_address","syscall","calltime")
attach(input)
times <- 1000000*calltime#Time is in microseconds now

pdf("System_calltime_histogram.pdf")
plot(hist.data, main='System call time vs. Frequency', ylab='Frequency (log10)',
xlab='Time spent in system call',xlim=c(0,450),freq=T)
dev.off()

#NOTE: If the last histogram bar "inverts" in the R interpreter, it might not invert
#in the actual output
